#!/usr/bin/env python

import casacore.tables
import numpy as np
import os
import subprocess
import unittest

# Settings
DATA_DIR = os.environ["DATA_DIR"]
MS_NAME = os.environ["MS_NAME"]
MS_DATA_COLUMN = "DATA"
MS_FLAG_COLUMN_IN = "FLAG"
MS_FLAG_COLUMN_REF = "FLAG_REF"
MS_FLAG_COLUMN_TST = "FLAG_TEST"
BINARY_DIR = os.environ["BINARY_DIR"]

# Test and configuration files
MS = os.path.join(DATA_DIR, MS_NAME)


class IntegrationTests(unittest.TestCase):
    def test_python(self):
        # Run directionalstat on the test data
        cmd = [
            f"{BINARY_DIR}/directionalstat.py",
            f"{MS}",
            f"{MS_DATA_COLUMN}",  # input data column
            f"{MS_FLAG_COLUMN_IN}",  # input flag column
            f"{MS_FLAG_COLUMN_TST}",  # output flag column
            "600",  # UV limit
            "60",  # time window
            "0.5",
        ]  # R threshold
        data = subprocess.run(cmd)
        assert data.returncode == 0, " ".join(cmd)

        # Open measurementset
        ms = casacore.tables.table(f"{MS}", readonly=False)

        # Read data columns
        col_ref = ms.getcol(MS_FLAG_COLUMN_REF)
        col_test = ms.getcol(MS_FLAG_COLUMN_TST)
        print(
            f"Comparing column '{MS_FLAG_COLUMN_REF} to '{MS_FLAG_COLUMN_TST}'"
        )

        # Remove test column
        ms.removecols(MS_FLAG_COLUMN_TST)

        # Compare data columns
        assert np.allclose(col_ref, col_test)

    def test_cpp(self):
        # Run directionalstat on the test data
        cmd = [
            f"{BINARY_DIR}/directionalstat-ms",
            f"{MS}",
            f"{MS_DATA_COLUMN}",  # input data column
            f"{MS_FLAG_COLUMN_IN}",  # input flag column
            f"{MS_FLAG_COLUMN_TST}",  # output flag column
            "600",  # UV limit
            "60",  # time window
            "0.5",
        ]  # R threshold
        data = subprocess.run(cmd)
        assert data.returncode == 0, " ".join(cmd)

        # Open measurementset
        ms = casacore.tables.table(f"{MS}", readonly=False)

        # Read data columns
        col_ref = ms.getcol(MS_FLAG_COLUMN_REF)
        col_test = ms.getcol(MS_FLAG_COLUMN_TST)
        print(
            f"Comparing column '{MS_FLAG_COLUMN_REF} to '{MS_FLAG_COLUMN_TST}'"
        )

        # Remove test column
        ms.removecols(MS_FLAG_COLUMN_TST)

        # Compare data columns
        assert np.allclose(col_ref, col_test)


if __name__ == "__main__":
    unittest.main()
