#include <xtensor/xtensor.hpp>

namespace io {
class IO {
 public:
  virtual size_t get_n_antennas() = 0;
  virtual size_t get_n_baselines() = 0;
  virtual xt::xtensor<double, 1> get_frequencies() = 0;
  virtual xt::xtensor<double, 3> get_uvw() = 0;
  virtual xt::xtensor<std::complex<float>, 4> get_visibilities() = 0;
  virtual xt::xtensor<bool, 2> get_flags() = 0;
  virtual void set_flags(const xt::xtensor<bool, 2>& flags) = 0;
};

};  // namespace io