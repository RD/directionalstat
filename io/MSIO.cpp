#include <casacore/casa/Containers/Record.h>
#include <casacore/casa/Utilities/LinearSearch.h>
#include <casacore/ms/MeasurementSets/MeasurementSet.h>
#include <casacore/tables/Tables/ArrayColumn.h>

#include <xtensor/xtensor.hpp>

#include "MSIO.h"
#include "../common/detail.h"

namespace detail {
size_t get_n_correlations(const casacore::MeasurementSet& ms,
                          const std::string& datacolumn_name) {
  return casacore::ArrayColumn<std::complex<float>>(ms, datacolumn_name)(0)
      .nelements();
}

size_t get_n_antennas(const std::string& ms_name) {
  return casacore::Table(ms_name + "/ANTENNA").nrow();
}

xt::xtensor<double, 1> get_frequencies(const std::string& ms_name) {
  casacore::Table channel_table(ms_name + "/SPECTRAL_WINDOW");
  casacore::ArrayColumn<double> channel_column(channel_table, "CHAN_FREQ");
  const size_t n_channels = channel_column.nrow();
  xt::xtensor<double, 1> frequencies({n_channels}, 0);
  for (size_t chan = 0; chan < n_channels; ++chan) {
    frequencies[chan] = channel_column.get(0).data()[chan];
  }
  return frequencies;
}

}  // namespace detail

namespace io {
namespace ms {

MSIO::MSIO(const std::string& ms_name)
    : ms_name_(ms_name),
      datacolumn_name_("DATA"),
      flagcolumn_in_name_("FLAG"),
      flagcolumn_out_name_(flagcolumn_in_name_) {
  frequencies_ = detail::get_frequencies(ms_name_);
  n_antennas_ = detail::get_n_antennas(ms_name_);
  n_baselines_ = detail::compute_n_baselines(get_n_antennas());
  initialize_baselines(n_baselines_);
}

void MSIO::initialize_baselines(size_t n_baselines) {
  baselines_.resize(n_baselines);
  baseline_indices_.resize(n_baselines);

  for (size_t bl = 0; bl < n_baselines; ++bl) {
    baselines_[bl] = detail::compute_baseline(n_antennas_, n_baselines, bl);
    const size_t antenna1 = baselines_[bl].first;
    const size_t antenna2 = baselines_[bl].second;
    const size_t baseline_index =
        detail::compute_baseline_index(antenna1, antenna2);
    baseline_indices_[bl] = baseline_index;
  }
}

xt::xtensor<double, 3> MSIO::get_uvw() {
  casacore::MeasurementSet ms(ms_name_);

  size_t n_antennas = get_n_antennas();
  size_t n_baselines = detail::compute_n_baselines(n_antennas);
  size_t n_time = ms.nrow() / n_baselines;

  xt::xtensor<double, 3> uvw({n_baselines, n_time, 3});
  const casacore::ArrayColumn<double> uvw_table(ms, "UVW");

  for (size_t time = 0; time < n_time; ++time) {
    for (size_t bl = 0; bl < n_baselines; ++bl) {
      const size_t rownr = baseline_indices_[bl] + time * n_baselines;

      casacore::Array<double> uvw_array;
      uvw_table.get(rownr, uvw_array);
      casacore::Array<double>::const_contiter uvw_ptr = uvw_array.cbegin();

      for (size_t i = 0; i < 3; ++i) {
        uvw(bl, time, i) = uvw_ptr[i];
      }
    }  // end for bl
  }    // end for time

  return uvw;
}

xt::xtensor<std::complex<float>, 4> MSIO::get_visibilities() {
  const casacore::MeasurementSet ms(ms_name_);

  const size_t n_baselines = n_baselines_;
  const size_t n_time = ms.nrow() / n_baselines_;
  const size_t n_channels = frequencies_.shape(0);
  const size_t n_correlations =
      detail::get_n_correlations(ms, datacolumn_name_);

  xt::xtensor<std::complex<float>, 4> visibilities(
      {n_baselines, n_time, n_channels, n_correlations});

  const casacore::ArrayColumn<std::complex<float>> visibilities_table(
      ms, datacolumn_name_);

  for (size_t time = 0; time < n_time; ++time) {
    for (size_t bl = 0; bl < n_baselines; ++bl) {
      const size_t rownr = baseline_indices_[bl] + time * n_baselines;

      casacore::Array<std::complex<float>> visibilities_array;
      visibilities_table.get(rownr, visibilities_array);
      casacore::Array<std::complex<float>>::const_contiter visibilities_ptr =
          visibilities_array.cbegin();

      for (size_t cor = 0; cor < n_correlations; ++cor) {
        for (size_t chan = 0; chan < n_channels; ++chan) {
          const size_t idx = chan * n_correlations + cor;
          visibilities(bl, time, chan, cor) = visibilities_ptr[idx];
        }  // end for chan
      }    // end for cor
    }      // end for bl
  }        // end for time

  return visibilities;
}

xt::xtensor<bool, 2> MSIO::get_flags() {
  const casacore::MeasurementSet ms(ms_name_);

  const size_t n_baselines = get_n_baselines();
  const size_t n_time = ms.nrow() / n_baselines;

  xt::xtensor<bool, 2> flags({n_baselines, n_time});

  const casacore::ArrayColumn<bool> flags_table(ms, flagcolumn_in_name_);

  for (size_t time = 0; time < n_time; ++time) {
    for (size_t bl = 0; bl < n_baselines; ++bl) {
      const size_t rownr = baseline_indices_[bl] + time * n_baselines;

      casacore::Array<bool> flags_array;
      flags_table.get(rownr, flags_array);
      casacore::Array<bool>::const_contiter flag_ptr = flags_array.cbegin();
      flags(bl, time) = flag_ptr[0];
    }  // end for bl
  }    // end for time

  return flags;
}

void MSIO::set_flags(const xt::xtensor<bool, 2>& flags) {
  casacore::MeasurementSet ms(ms_name_);
  ms.reopenRW();

  const size_t n_baselines = flags.shape(0);
  const size_t n_time = flags.shape(1);

  const size_t n_correlations =
      detail::get_n_correlations(ms, datacolumn_name_);
  const size_t n_channels = frequencies_.shape(0);

  // add output flag column
  if (!ms.tableDesc().isColumn(flagcolumn_out_name_)) {
    casacore::Record dminfo = ms.dataManagerInfo();
    casacore::Record colinfo;
    for (size_t i = 0; i < dminfo.nfields(); ++i) {
      const casacore::Record& subrec = dminfo.subRecord(i);
      if (linearSearch1(casacore::Vector<casacore::String>(
                            subrec.asArrayString("COLUMNS")),
                        "FLAG") >= 0) {
        colinfo = subrec;
        break;
      }
    }
    if (colinfo.nfields() == 0) {
      throw std::runtime_error("Could not obtain column info");
    }
    casacore::TableDesc td;
    casacore::ColumnDesc cd = ms.tableDesc().columnDesc(flagcolumn_in_name_);
    td.addColumn(cd, flagcolumn_out_name_);
    colinfo.define("NAME", flagcolumn_out_name_ + "_dm");
    ms.addColumn(td, colinfo);
  }
  casacore::ArrayColumn<bool> flags_table_out(ms, flagcolumn_out_name_);

  for (size_t bl = 0; bl < n_baselines; ++bl) {
    for (size_t time = 0; time < n_time; ++time) {
      const size_t rownr = baseline_indices_[bl] + time * n_baselines;

      casacore::Array<bool> flags_array;
      flags_table_out.get(rownr, flags_array);
      casacore::Array<bool>::contiter flag_ptr = flags_array.cbegin();

      for (size_t cor = 0; cor < n_correlations; ++cor) {
        for (size_t chan = 0; chan < n_channels; ++chan) {
          const size_t idx = chan * n_correlations + cor;
          flag_ptr[idx] = flags(bl, time);
        }
      }

      flags_table_out.acbPut(rownr, flags_array);
    }  // end for time
  }    // end for bl
};

}  // namespace ms
}  // namespace io