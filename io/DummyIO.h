#include "IO.h"

namespace io {
namespace dummy {
class DummyIO : public IO {
 public:
  DummyIO(size_t n_antennas, size_t n_time, size_t n_channels);
  virtual size_t get_n_antennas() override { return n_antennas_; };
  virtual size_t get_n_baselines() override { return n_baselines_; };
  virtual xt::xtensor<double, 1> get_frequencies() override {
    return frequencies_;
  };
  virtual xt::xtensor<double, 3> get_uvw() override;
  virtual xt::xtensor<std::complex<float>, 4> get_visibilities() override;
  virtual xt::xtensor<bool, 2> get_flags() override;
  virtual void set_flags(const xt::xtensor<bool, 2>& flags) override{};

 private:
  size_t n_antennas_ = 0;
  size_t n_time_ = 0;
  size_t n_baselines_ = 0;
  std::vector<size_t> baseline_indices_;
  xt::xtensor<double, 1> frequencies_;
};
}  // namespace dummy
}  // namespace io