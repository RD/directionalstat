#include <ctime>
#include <xtensor/xrandom.hpp>

#include "DummyIO.h"
#include "../common/detail.h"

namespace detail {
xt::xtensor<double, 1> get_frequencies(size_t n_channels) {
  xt::xtensor<double, 1> frequencies({n_channels}, 0);
  const float start_frequency = 150e6;    // MHz
  const float frequency_increment = 1e6;  // MHz
  for (size_t chan = 0; chan < n_channels; ++chan) {
    frequencies[chan] = start_frequency + frequency_increment * chan;
  }
  return frequencies;
}
}  // namespace detail

namespace io {
namespace dummy {
DummyIO::DummyIO(size_t n_antennas, size_t n_time, size_t n_channels)
    : n_antennas_(n_antennas), n_time_(n_time) {
  xt::random::seed(0);
  frequencies_ = detail::get_frequencies(n_channels);
  n_baselines_ = detail::compute_n_baselines(get_n_antennas());
  baseline_indices_ = detail::compute_baseline_selection(n_baselines_);
}

xt::xtensor<double, 3> DummyIO::get_uvw() {
  return xt::random::randn<double>(
      {n_baselines_, n_time_, static_cast<size_t>(3)});
}

xt::xtensor<std::complex<float>, 4> DummyIO::get_visibilities() {
  const size_t n_channels = frequencies_.shape(0);
  const size_t n_correlations = 4;
  xt::xtensor<std::complex<float>, 4> visibilites(
      {n_baselines_, n_time_, n_channels, n_correlations});

  for (size_t bl = 0; bl < n_baselines_; ++bl) {
    for (size_t time = 0; time < n_time_; ++time) {
      for (size_t chan = 0; chan < n_channels; ++chan) {
        auto r = xt::random::rand<float>({n_correlations});
        for (size_t cor = 0; cor < n_correlations; ++cor) {
          visibilites(bl, time, chan, cor) = {1.0f + r[cor], 0};
        }
      }
    }
  }

  return visibilites;
}

xt::xtensor<bool, 2> DummyIO::get_flags() {
  return xt::xtensor<bool, 2>({n_baselines_, n_time_}, false);
}
}  // namespace dummy
}  // namespace io
