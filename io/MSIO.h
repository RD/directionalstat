#include "IO.h"

namespace io {
namespace ms {
class MSIO : public IO {
 public:
  MSIO(const std::string& ms_name);
  virtual size_t get_n_antennas() override { return n_antennas_; };
  virtual size_t get_n_baselines() override { return n_baselines_; };
  virtual xt::xtensor<double, 1> get_frequencies() override {
    return frequencies_;
  };
  virtual xt::xtensor<double, 3> get_uvw() override;
  virtual xt::xtensor<std::complex<float>, 4> get_visibilities() override;
  virtual xt::xtensor<bool, 2> get_flags() override;
  virtual void set_flags(const xt::xtensor<bool, 2>& flags) override;

  void set_datacolumn(const std::string datacolumn_name) {
    datacolumn_name_ = datacolumn_name;
  };
  void set_flagcolumn_in(const std::string flagcolumn_name) {
    flagcolumn_in_name_ = flagcolumn_name;
  };
  void set_flagcolumn_out(const std::string flagcolumn_name) {
    flagcolumn_out_name_ = flagcolumn_name;
  };

 private:
  void initialize_baselines(size_t);

  const std::string& ms_name_;   // name of measurementset
  std::string datacolumn_name_;  // name of visibility data column (e.g. DATA)
  std::string flagcolumn_in_name_;   // name of flag column (e.g. FLAG)
  std::string flagcolumn_out_name_;  // name of flag column (e.g. FLAG)
  std::vector<std::pair<size_t, size_t>> baselines_;
  std::vector<size_t> baseline_indices_;
  size_t n_antennas_ = 0;
  size_t n_baselines_ = 0;
  xt::xtensor<double, 1> frequencies_;
};
}  // namespace ms
}  // namespace io