#include <iostream>
#include <string>

#include <xtensor/xtensor.hpp>
#include <xtensor/xadapt.hpp>
#include <xtensor/xview.hpp>

#include "algorithm/DirectionalstatCPU.h"
#if defined(HAVE_CUDA)
#include "algorithm/DirectionalstatCUDA.h"
#endif
#include "common/detail.h"
#include "external/wsclean/stopwatch.h"
#include "io/MSIO.h"

void directional_stat(const std::string& ms_name,
                      const std::string& data_column,
                      const std::string& flag_column_in,
                      const std::string& flag_column_out, float uv_limit,
                      size_t time_step, float R_threshold) {
  std::cout << ">>> Parameters"
            << "\n";
  std::cout << "ms_name        : " << ms_name << "\n";
  std::cout << "data_column    : " << data_column << "\n";
  std::cout << "flag_column_in : " << flag_column_in << "\n";
  std::cout << "flag_column_out: " << flag_column_out << "\n";
  std::cout << "uv_limit       : " << uv_limit << "\n";
  std::cout << "time_step      : " << time_step << "\n";
  std::cout << "R_threshold    : " << R_threshold << "\n";
  std::cout << "\n";

  // input data
  Stopwatch stopwatch_input(true);
  io::ms::MSIO io(ms_name);
  io.set_datacolumn(data_column);
  io.set_flagcolumn_in(flag_column_in);
  io.set_flagcolumn_out(flag_column_out);
  xt::xtensor<double, 1> frequencies = io.get_frequencies();
  xt::xtensor<std::complex<float>, 4> visibilities = io.get_visibilities();
  xt::xtensor<double, 3> uvw = io.get_uvw();
  const xt::xtensor<bool, 2> flags_in = io.get_flags();
  stopwatch_input.Pause();

  // parameters
  const size_t n_channels = frequencies.shape(0);
  const double frequency_first = frequencies(0);
  const size_t n_antennas = io.get_n_antennas();
  const size_t n_baselines = io.get_n_baselines();
  const size_t n_time = visibilities.shape(1);
  const size_t n_correlations = visibilities.shape(3);

  // initialization
  xt::xtensor<std::pair<size_t, size_t>, 1> baselines =
      detail::compute_baselines(n_antennas);
  assert(n_baselines == baselines.shape(0));

  // data selection
  Stopwatch stopwatch_selection(true);
  std::vector<size_t> baseline_indices = detail::compute_baseline_selection(
      uvw, baselines, uv_limit, frequencies(0));
  const size_t n_baselines_selected = baseline_indices.size();
  auto flags_slice = xt::view(flags_in, xt::keep(baseline_indices), xt::all(),
                              xt::newaxis(), xt::newaxis());
  xt::xtensor<std::complex<float>, 4> visibilities_slice =
      xt::view(visibilities, xt::keep(baseline_indices), xt::all(), xt::all(),
               xt::all());
  visibilities_slice *= (1 - flags_slice);
  assert(n_baselines_selected == visibilities_slice.shape(0));
  stopwatch_selection.Pause();

  // report parameters
  std::cout << ">>> Parameters"
            << "\n";
  std::cout << "n_channels    : " << n_channels << "\n";
  std::cout << "frequency0    : " << frequency_first << "\n";
  std::cout << "n_antennas    : " << n_antennas << "\n";
  std::cout << "n_baselines   : " << n_baselines_selected << "/" << n_baselines
            << "\n";
  std::cout << "n_time        : " << n_time << "\n";
  std::cout << "n_correlations: " << n_correlations << "\n";
  std::cout << "\n";

  // initialization
  xt::xtensor<bool, 2> flags_out({n_baselines, n_time}, false);
  auto ds = detail::get_directionalstat(visibilities_slice, flags_out,
                                        baseline_indices, time_step);
  ds->initialize();

  // flagging
#if defined(HAVE_PMT)
  std::unique_ptr<pmt::PMT> pmt = detail::get_pmt();
  pmt::State startState = pmt->read();
#endif
  Stopwatch stopwatch_compute(true);
  ds->compute_statistics();
  ds->finish();
  stopwatch_compute.Pause();
#if defined(HAVE_PMT)
  pmt::State endState = pmt->read();
#endif

  // write flags
  Stopwatch stopwatch_output(true);
  ds->compute_flags(R_threshold);
  ds->finish();
  io.set_flags(flags_in + flags_out);
  stopwatch_output.Pause();

  std::cout << ">>> Results"
            << "\n";
  detail::report_rfi(flags_in, flags_out, n_baselines_selected, time_step);
  std::cout << "time spent reading input : " << stopwatch_input.ToShortString()
            << "\n";
  std::cout << "time spent copying data  : "
            << stopwatch_selection.ToShortString() << "\n";
  std::cout << "time spent flagging      : "
            << stopwatch_compute.ToShortString() << "\n";
  std::cout << "time spent writing output: " << stopwatch_output.ToShortString()
            << "\n";

#if defined(HAVE_PMT)
  std::cout << "flagging energy          : "
            << pmt::PMT::joules(startState, endState) << "J ("
            << pmt::PMT::watts(startState, endState) << "W avg.)"
            << "\n";
#endif
}  // end directional_stat

int main(int argc, char* argv[]) {
  if (argc > 7) {
    const std::string ms_name = argv[1];
    const std::string data_column = argv[2];
    const std::string flag_column_in = argv[3];
    const std::string flag_column_out = argv[4];
    const float uv_limit = atof(argv[5]);
    const int time_step = atoi(argv[6]);
    const float R_threshold = atof(argv[7]);
    directional_stat(ms_name, data_column, flag_column_in, flag_column_out,
                     uv_limit, time_step, R_threshold);
  } else {
    printf(
        "Usage: %s MS datacol flagcol_in flagcol_out uvcut(lambda) "
        "timewindow(samples) "
        "threshold(0.0-1.0)\n",
        argv[0]);
    printf("Example: %s MS DATA FLAG FLAG_OUT 600 60 0.5\n", argv[0]);
  }
}  // end main
