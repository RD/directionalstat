#include <iostream>
#include <string>
#include <thread>

#include <xtensor/xtensor.hpp>

#include "common/detail.h"
#include "external/wsclean/stopwatch.h"
#include "external/wsclean/timestring.h"
#include "io/DummyIO.h"

// Enable this line to run the statistics kernel only once.
// This feature is intended to be used for profiling purposes.
// #define KERNEL_PROFILING

void directional_stat(size_t n_antennas, size_t n_time, size_t n_channels,
                      size_t time_step, float R_threshold) {
  std::cout << ">>> Parameters"
            << "\n";
  std::cout << "time_step      : " << time_step << "\n";
  std::cout << "R_threshold    : " << R_threshold << "\n";
  std::cout << "\n";

  // input data
  Stopwatch stopwatch_input(true);
  io::dummy::DummyIO io(n_antennas, n_time, n_channels);
  xt::xtensor<double, 1> frequencies = io.get_frequencies();
  assert(n_channels == frequencies.shape(0));
  xt::xtensor<std::complex<float>, 4> visibilities = io.get_visibilities();
  assert(n_time == visibilities.shape(1));
  assert(n_channels == visibilities.shape(2));
  stopwatch_input.Pause();

  // parameters
  const double frequency_first = frequencies(0);
  const size_t n_baselines = io.get_n_baselines();
  assert(n_baselines == visibilities.shape(0));
  const size_t n_correlations = visibilities.shape(3);

  std::cout << ">>> Parameters"
            << "\n";
  std::cout << "n_channels    : " << n_channels << "\n";
  std::cout << "frequency0    : " << frequency_first << "\n";
  std::cout << "n_antennas    : " << n_antennas << "\n";
  std::cout << "n_baselines   : " << n_baselines << "\n";
  std::cout << "n_time        : " << n_time << "\n";
  std::cout << "n_correlations: " << n_correlations << "\n";
  std::cout << "\n";

  // energy measurement
#if defined(HAVE_PMT)
  std::vector<std::unique_ptr<pmt::PMT>> pmts;
  for (size_t i = 0; i < 3; ++i) {
    pmts.push_back(detail::get_pmt(i));
  }
  std::vector<pmt::State> startStates;
  std::vector<pmt::State> endStates;
#endif

  // flagging
  xt::xtensor<bool, 2> flags({n_baselines, n_time}, false);
  auto ds = detail::get_directionalstat(visibilities, flags, time_step);
  ds->initialize();
#if defined(KERNEL_PROFILING)
  Stopwatch stopwatch_compute(true);
  const size_t nr_iterations = 1;
  ds->compute_statistics();
  ds->finish();
  stopwatch_compute.Pause();
#else
  volatile bool stop = false;
  volatile bool count = false;
  size_t nr_iterations = 0;
  std::thread thread([&]() {
    size_t i = 0;
    while (!stop) {
      ds->compute_statistics();
      if (count) {
        nr_iterations++;
      }
      if (i++ % 10 == 0) {
        ds->finish();
      }
    }
  });
  std::this_thread::sleep_for(std::chrono::seconds(1));
  Stopwatch stopwatch_compute(true);
#if defined(HAVE_PMT)
  for (auto& pmt : pmts) {
    startStates.push_back(pmt->read());
  }
#endif
  count = true;
  std::this_thread::sleep_for(std::chrono::seconds(3));
  stop = true;
  ds->finish();
  stopwatch_compute.Pause();
#if defined(HAVE_PMT)
  for (auto& pmt : pmts) {
    endStates.push_back(pmt->read());
  }
#endif
  if (thread.joinable()) {
    thread.join();
  }
#endif

  // write flags
  Stopwatch stopwatch_output(true);
  ds->compute_flags(R_threshold);
  io.set_flags(flags);
  stopwatch_output.Pause();

  const double runtime_input = stopwatch_input.Seconds();
  const double runtime_compute = stopwatch_compute.Seconds() / nr_iterations;
  const size_t nr_visibilities = visibilities.size() / n_correlations;
  std::cout << ">>> Results"
            << "\n";
  std::cout << "time spent making input : " << ToShortString(runtime_input)
            << "\n";
  std::cout << "time spent flagging     : " << ToShortString(runtime_compute)
            << "\n";
  std::cout << "flagging throughput     : "
            << (nr_visibilities / runtime_compute) * 1e-6 << " MVis/s"
            << "\n";
  const size_t sizeof_visibilities =
      visibilities.size() * sizeof(std::complex<float>);
  std::cout << "read bandwidth          : "
            << (sizeof_visibilities / runtime_compute) / (1024 * 1024 * 1024)
            << " GB/s"
            << "\n";

#if defined(HAVE_PMT) && !defined(KERNEL_PROFILING)
  for (size_t i = 0; i < pmts.size(); ++i) {
    const double joules =
        pmt::PMT::joules(startStates[i], endStates[i]) / nr_iterations;
    const double watts = pmt::PMT::watts(startStates[i], endStates[i]);
    if (joules > 0) {
      std::cout << "energy usage            : " << joules << "J (" << watts
                << "W avg.)"
                << "\n";
    }
  }
#endif
}  // end directional_stat

int main(int argc, char* argv[]) {
  if (argc > 5) {
    const size_t n_antennas = atoi(argv[1]);
    const size_t n_time = atoi(argv[2]);
    const size_t n_channels = atoi(argv[3]);
    const int time_step = atoi(argv[4]);
    const float R_threshold = atof(argv[5]);
    directional_stat(n_antennas, n_time, n_channels, time_step, R_threshold);
  } else {
    printf(
        "Usage: %s nantennas ntime nchannels "
        "timewindow(samples) "
        "threshold(0.0-1.0)\n",
        argv[0]);
    printf("Example: %s 55 180 1 60 0.5\n", argv[0]);
  }
}  // end main
