#!/usr/bin/env python
#
# Copyright (C) 2016- Sarod Yatawatta <sarod@users.sf.net>
# Copyright (C) 2022- Bram Veenboer <veenboer@astron.nl>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#


import argparse
from pyrap.tables import *
import math
import numpy
from time import time_ns


def get_visibilities(visibilities, flags, pol):
    n_time = visibilities.shape[0]
    # flag=True means value is 1, make all flagged values zero
    return (1 - flags[0:n_time, :, pol]) * visibilities[0:n_time, :, pol]


def compute_r(XX, XY, YX, YY, time_first, time_last):
    # one chunk per time
    # form Stokes sub-vector
    QQ = XX[time_first:time_last, :] - YY[time_first:time_last, :]
    UU = XY[time_first:time_last, :] + YX[time_first:time_last, :]
    VV = 1j * (XY[time_first:time_last, :] - YX[time_first:time_last, :])

    # flatten data
    QQ = QQ.flatten()
    UU = UU.flatten()
    VV = VV.flatten()

    # norm of full vector
    PP = numpy.sqrt(
        numpy.multiply(QQ, QQ.conjugate())
        + numpy.multiply(UU, UU.conjugate())
        + numpy.multiply(VV, VV.conjugate())
    )

    # normalize
    QQ = numpy.divide(QQ, abs(PP) + 1e-6)
    UU = numpy.divide(UU, abs(PP) + 1e-6)
    VV = numpy.divide(VV, abs(PP) + 1e-6)

    # extract point on Poincare sphere
    # sum of direction cosines
    zeta1 = numpy.sum(numpy.real(QQ))
    zeta2 = numpy.sum(numpy.real(UU))
    zeta3 = numpy.sum(numpy.real(VV))

    n_samples = QQ.shape[0] + 1

    # vector sum of radii, normalized by number of samples
    R = math.sqrt(zeta1 * zeta1 + zeta2 * zeta2 + zeta3 * zeta3) / n_samples

    return R


def compute_confidence(R, time_window):
    # find 5% confidence interval for this value of R (pp. 303 Fisher paper)
    return math.degrees(
        math.acos(
            1.0
            - (1.0 - R)
            / R
            * (math.pow(100.0 / 5.0, 1.0 / (time_window - 1.0)) - 1.0)
        )
    )


def compute_baseline_index(antenna1, antenna2):
    row = min(antenna1, antenna2)
    col = max(antenna1, antenna2)
    return int((col * (col + 1)) / 2 + row)


def compute_n_baselines(n_antennas):
    n_correlations = (n_antennas * (n_antennas - 1)) // 2
    n_autocorrelations = n_antennas
    return n_correlations + n_autocorrelations


def compute_baseline(baseline_index, n_antennas):
    n_baselines = compute_n_baselines(n_antennas)
    antenna = (1 + math.sqrt(1 + 8 * (n_baselines - baseline_index - 1))) // 2
    antenna1 = n_antennas - antenna
    # n is the number of baselines (a,b) with a < antenna
    n = n_baselines - n_antennas - (antenna * (antenna - 1)) // 2
    antenna2 = baseline_index - n
    return (antenna1, antenna2)


def directional_stat(
    ms_name,
    data_column,
    flag_column_in,
    flag_column_out,
    uv_limit,
    time_window,
    R_threshold,
    enable_timings=False,
):
    t = table(ms_name, readonly=False)

    # get frequency of first channel
    chan = table(ms_name + "/SPECTRAL_WINDOW").getcol("CHAN_FREQ")
    n_channels = len(chan)
    frequency_first = chan[0, 0]
    # wavelength
    lmd = 299792458.0 / frequency_first

    # add output flag column
    if not flag_column_out in t.colnames():
        out_column_desc = t.getcoldesc(flag_column_in)
        out_column_desc["name"] = flag_column_out
        t.addcols((out_column_desc))
    t.putcol(flag_column_out, t.getcol(flag_column_in))

    # antennas
    n_antennas = table(ms_name + "/ANTENNA").nrows()

    # enable this to calculate confidence intervals for each detection
    enable_confinterval = False

    # enable this to report timings
    time_input = 0
    time_compute = 0
    time_output = 0

    # max possible baselines (including auto correlations)
    n_baselines = compute_n_baselines(n_antennas)
    n_time = t.nrows() // n_baselines

    # keep track of which baselines are selected and how
    # many chunks are flagged for every baseline
    baseline_selected = np.zeros(n_baselines, dtype=bool)
    baseline_n_chunks = np.zeros(n_baselines, dtype=int)

    # data columns for all baselines
    visibilities = t.getcol(data_column)
    assert n_channels == visibilities.shape[1]
    n_correlations = visibilities.shape[2]
    flags = t.getcol("FLAG")
    uvw = t.getcol("UVW")

    for bl in range(n_baselines):
        antenna1, antenna2 = compute_baseline(bl, n_antennas)
        if antenna1 == antenna2:
            continue

        # data for current baseline
        visibilities_bl = np.zeros(
            shape=(n_time, n_channels, n_correlations),
            dtype=visibilities.dtype,
        )
        flags_bl = np.zeros(
            shape=(n_time, n_channels, n_correlations), dtype=flags.dtype
        )
        uvw_bl = np.zeros(shape=(n_time, uvw.shape[1]), dtype=uvw.dtype)

        baseline_index = compute_baseline_index(antenna1, antenna2)
        time_input -= time_ns()
        for time in range(n_time):
            index = baseline_index + time * n_baselines
            visibilities_bl[time, :, :] = visibilities[index, :, :]
            flags_bl[time, :, :] = flags[index, :, :]
            uvw_bl[time, :] = uvw[index, :]
        time_input += time_ns()

        uu = uvw_bl[:, 0]
        vv = uvw_bl[:, 1]
        ww = uvw_bl[:, 2]

        # select only baselines that fall within the range
        uv_distance = numpy.sqrt(uu * uu + vv * vv + ww * ww)
        uv_distance_mean = uv_distance.mean() / lmd  # in wavelengths
        if uv_distance_mean < 0 or uv_distance_mean > uv_limit:
            continue

        time_compute -= time_ns()

        baseline_selected[bl] = True

        XX = get_visibilities(visibilities_bl, flags_bl, 0)
        XY = get_visibilities(visibilities_bl, flags_bl, 1)
        YX = get_visibilities(visibilities_bl, flags_bl, 2)
        YY = get_visibilities(visibilities_bl, flags_bl, 3)

        for time_first in range(0, n_time, time_window):
            time_last = min(time_first + time_window - 1, n_time)
            R = compute_r(XX, XY, YX, YY, time_first, time_last)

            if R >= R_threshold:
                flags_bl[time_first:time_last, :, :] = True
                baseline_n_chunks[bl] += 1
                if enable_confinterval:
                    print(f"confidence: {compute_confidence(R, time_window)}")

        time_compute += time_ns()

        time_output -= time_ns()
        for time in range(n_time):
            index = baseline_index + time * n_baselines
            t.putcell(flag_column_out, index, flags_bl[time, :, :])
        time_output += time_ns()

    t.close()

    # number of time windows
    n_windows = (n_time + time_window - 1) // time_window

    # number of baselines selected
    n_baselines_selected = sum(baseline_selected)

    # number of baselines with rfi
    n_baselines_rfi = len(baseline_n_chunks[baseline_n_chunks > 0])

    # number of chunks with rfi
    n_chunks_rfi = sum(baseline_n_chunks)

    print(f"Inspected {n_baselines_selected} out of {n_baselines} baselines")
    print(f"Found RFI in {n_baselines_rfi} baselines")
    print(
        f"Found RFI in {n_chunks_rfi} out of {n_baselines_selected*n_windows} windows"
    )

    if enable_timings:
        time_total = time_input + time_compute + time_output
        for timing in [
            (time_input, "reading input"),
            (time_compute, "flagging"),
            (time_output, "writing output"),
        ]:
            percentage = timing[0] / time_total * 100
            print(
                f"time spent {timing[1]:14}:  {timing[0]*1e-9:.2f} s ({percentage:4.1f} %)"
            )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("MS", help="input measurementset", type=str)
    parser.add_argument(
        "DATA", help="input data column", default="DATA", type=str
    )
    parser.add_argument(
        "FLAG_IN", help="input flag column", default="FLAG", type=str
    )
    parser.add_argument("FLAG_OUT", help="output flag column", type=str)
    parser.add_argument("uv_limit", help="uv cut (lambda)", type=float)
    parser.add_argument(
        "time_window", help="window size (# samples)", type=int
    )
    parser.add_argument(
        "threshold", help="flagging threshold (0.0-1.0)", type=float
    )
    parser.add_argument(
        "-timings", help="report timings", default=False, action="store_true"
    )
    args = parser.parse_args()

    directional_stat(
        args.MS,
        args.DATA,
        args.FLAG_IN,
        args.FLAG_OUT,
        args.uv_limit,
        args.time_window,
        args.threshold,
        args.timings,
    )
