# Introduction
This repository has the code (`directionalstat.py`) belonging to the "Polarization-based online interference mitigation in radio interferometry" [paper](https://ieeexplore.ieee.org/document/9287467/) by Sarod Yatawatta.

# Usage
Using the `sm.ms.tar` dataset (md5 b5a325cc) provided by Sarod, the script can be executed as follows:
```
$ python directionalstat.py sm.ms DATA MODEL_DATA 600 60 0.5
```
The C++ implementation depends on the schaapcommon repository. Once this repository is cloned, dependencies can be pulled with:
```
$ git submodule init
$ git submodule update
```

The C++ implementation needs to be build first:
```
$ mkdir build
$ cd build
$ cmake ..
$ make
```

Next, in can be executed like the Python script:
```
./directionalstat sm.ms DATA MODEL_DATA 600 60 0.5
```

# Testing
To check correctness of the code (e.g. after making modifications), you can use the `test-directionalstat.py` test script. This test assumes that you first executed a known 'good' version of `directionalstat.py` using the aforementioned parameters to populate the `MODEL_DATA` column. The test runs the script again (using a different output column) and checks whether the data is the same.