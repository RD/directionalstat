#include <string>

std::string ToString(double seconds);
std::string ToShortString(double seconds);
std::string ToDaysString(double seconds);
std::string ToHoursString(double seconds);
std::string ToMinutesString(double seconds);
std::string ToSecondsString(double seconds);
std::string ToMilliSecondsString(double seconds);
std::string ToMicroSecondsString(double seconds);
std::string ToNanoSecondsString(double seconds);
