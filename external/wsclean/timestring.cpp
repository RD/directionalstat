#include <boost/date_time/posix_time/posix_time.hpp>

#include "timestring.h"

std::string ToString(double seconds) {
  return to_simple_string(
      boost::posix_time::seconds(static_cast<long>(roundl(seconds))));
}

std::string ToShortString(double seconds) {
  if (seconds >= 60 * 60 * 24)
    return ToDaysString(seconds);
  else if (seconds >= 60 * 60)
    return ToHoursString(seconds);
  else if (seconds >= 60)
    return ToMinutesString(seconds);
  else if (seconds >= 1.0)
    return ToSecondsString(seconds);
  else if (seconds >= 0.001)
    return ToMilliSecondsString(seconds);
  else if (seconds >= 0.000001)
    return ToMicroSecondsString(seconds);
  else
    return ToNanoSecondsString(seconds);
}

std::string ToDaysString(double seconds) {
  const long double days = roundl(seconds / (60.0 * 60.0)) / 24.0;
  std::stringstream str;
  if (days >= 10.0)
    str << roundl(days) << " days";
  else
    str << floorl(days) << 'd' << (days * 24.0) << 'h';
  return str.str();
}

std::string ToHoursString(double seconds) {
  const long double hours = roundl(seconds / 60.0) / 60.0;
  std::stringstream str;
  if (hours >= 10.0)
    str << roundl(hours) << 'h';
  else
    str << floorl(hours) << 'h' << (hours * 60.0) << 'm';
  return str.str();
}

std::string ToMinutesString(double seconds) {
  const long double mins = roundl(seconds) / 60.0;
  std::stringstream str;
  if (mins >= 10.0)
    str << roundl(mins) << " min";
  else
    str << floorl(mins) << 'm' << fmod(mins * 60.0, 60.0) << 's';
  return str.str();
}

std::string ToSecondsString(double seconds) {
  seconds = roundl(seconds * 10.0) / 10.0;
  std::stringstream str;
  if (seconds >= 10.0)
    str << roundl(seconds) << 's';
  else
    str << seconds << 's';
  return str.str();
}

std::string ToMilliSecondsString(double seconds) {
  const long double msec = roundl(seconds * 10000.0) / 10.0;
  std::stringstream str;
  if (msec >= 10.0)
    str << roundl(seconds * 1000.0) << "ms";
  else
    str << msec << "ms";
  return str.str();
}

std::string ToMicroSecondsString(double seconds) {
  const long double usec = roundl(seconds * 10000000.0) / 10.0;
  std::stringstream str;
  if (usec >= 10.0)
    str << roundl(seconds * 1000000.0) << "µs";
  else
    str << usec << "µs";
  return str.str();
}

std::string ToNanoSecondsString(double seconds) {
  const long double nsec = roundl(seconds * 10000000000.0) / 10.0;
  std::stringstream str;
  if (nsec >= 10.0)
    str << roundl(seconds * 1000000000.0) << "ns";
  else
    str << nsec << "ns";
  return str.str();
}
