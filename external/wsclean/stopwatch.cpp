#include "stopwatch.h"
#include "timestring.h"

#include <cmath>
#include <sstream>

#include <boost/date_time/posix_time/posix_time.hpp>

Stopwatch::Stopwatch() : _running(false), _sum(boost::posix_time::seconds(0)) {}

Stopwatch::Stopwatch(bool start)
    : _running(start),
      _startTime(boost::posix_time::microsec_clock::local_time()),
      _sum(boost::posix_time::seconds(0)) {}

Stopwatch::~Stopwatch() {}

void Stopwatch::Start() {
  if (!_running) {
    _startTime = boost::posix_time::microsec_clock::local_time();
    _running = true;
  }
}

void Stopwatch::Pause() {
  if (_running) {
    _sum += (boost::posix_time::microsec_clock::local_time() - _startTime);
    _running = false;
  }
}

void Stopwatch::Reset() {
  _running = false;
  _sum = boost::posix_time::seconds(0);
}

std::string Stopwatch::ToString() const {
  if (_running) {
    boost::posix_time::time_duration current =
        _sum + (boost::posix_time::microsec_clock::local_time() - _startTime);
    return to_simple_string(current);
  } else {
    return to_simple_string(_sum);
  }
}

std::string Stopwatch::ToShortString() const {
  return ::ToShortString(Seconds());
}

long double Stopwatch::Seconds() const {
  if (_running) {
    boost::posix_time::time_duration current =
        _sum + (boost::posix_time::microsec_clock::local_time() - _startTime);
    return (long double)current.total_microseconds() / 1000000.0;
  } else {
    return (long double)_sum.total_microseconds() / 1000000.0;
  }
}
