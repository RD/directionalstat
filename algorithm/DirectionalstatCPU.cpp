#include "common/detail.h"

#include "DirectionalstatCPU.h"

namespace algorithm {
namespace cpu {

Directionalstat::Directionalstat(
    xt::xtensor<std::complex<float>, 4>& visibilities,
    xt::xtensor<bool, 2>& flags, size_t time_step)
    : algorithm::Directionalstat(visibilities, flags), time_step_(time_step) {
  const size_t n_baselines = visibilities.shape(0);
  baseline_indices_ = detail::compute_baseline_selection(n_baselines);
}

Directionalstat::Directionalstat(
    xt::xtensor<std::complex<float>, 4>& visibilities,
    xt::xtensor<bool, 2>& flags,

    const std::vector<size_t> baseline_indices, size_t time_step)
    : algorithm::Directionalstat(visibilities, flags),
      time_step_(time_step),
      baseline_indices_(baseline_indices) {}

void Directionalstat::compute_statistics() {
  const size_t n_baselines = visibilities_.shape(0);
  const size_t n_time = visibilities_.shape(1);
  const size_t n_channels = visibilities_.shape(2);
  const size_t n_windows = (n_time + time_step_ - 1) / time_step_;

  R_ = xt::xtensor<double, 3>({n_baselines, n_windows, n_channels}, 0);

#pragma omp parallel for
  for (size_t bl = 0; bl < n_baselines; ++bl) {
    for (size_t window = 0; window < n_windows; ++window) {
      const size_t time_first = window * time_step_;
      const size_t time_last = std::min(time_first + time_step_ - 1, n_time);

      for (size_t chan = 0; chan < n_channels; ++chan) {
        // sum of direction cosines
        float zeta1 = 0;
        float zeta2 = 0;
        float zeta3 = 0;

        for (size_t time = time_first; time < time_last; ++time) {
          const std::complex<float> XX = visibilities_(bl, time, chan, 0);
          const std::complex<float> XY = visibilities_(bl, time, chan, 1);
          const std::complex<float> YX = visibilities_(bl, time, chan, 2);
          const std::complex<float> YY = visibilities_(bl, time, chan, 3);

          // form Stokes
          std::complex<float> QQ = XX - YY;
          std::complex<float> UU = XY + YX;
          const std::complex<float> temp = XY - YX;
          std::complex<float> VV(temp.imag(), temp.real());

          // norm
          const std::complex<float> PP = std::sqrt(
              QQ * std::conj(QQ) + UU * std::conj(UU) + VV * std::conj(VV));

          // normalize
          QQ /= std::abs(PP) + 1e-6;
          UU /= std::abs(PP) + 1e-6;
          VV /= std::abs(PP) + 1e-6;

          // extract point on Poincare sphere
          zeta1 += QQ.real();
          zeta2 += UU.real();
          zeta3 += VV.real();
        }

        // vector sum of radii, normalized by number of samples
        const size_t n_samples = (time_last - time_first) * n_channels + 1;
        R_(bl, window, chan) =
            std::sqrt(zeta1 * zeta1 + zeta2 * zeta2 + zeta3 * zeta3) /
            n_samples;
      }  // end for chan
    }    // end for time_window
  }      // end for bl
}  // end compute_r

void Directionalstat::compute_flags(float R_threshold) {
  const size_t n_baselines = visibilities_.shape(0);
  const size_t n_time = flags_.shape(1);

#pragma omp parallel for
  for (size_t bl = 0; bl < n_baselines; ++bl) {
    const size_t bl_dst = baseline_indices_[bl];
    for (size_t time_first = 0; time_first < n_time; time_first += time_step_) {
      const size_t time_last = std::min(time_first + time_step_ - 1, n_time);
      const size_t time_window = time_first / time_step_;
      if (R_(bl, time_window, 0) > R_threshold) {
        for (size_t time = time_first; time < time_last; ++time) {
          flags_(bl_dst, time) = true;
        }
      }
    }  // end for time_first
  }    // end for bl
}  // end update_flags

}  // namespace cpu
}  // namespace algorithm
