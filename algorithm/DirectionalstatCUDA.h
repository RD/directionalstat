#ifndef DIRECTIONALSTATCUDA_H_
#define DIRECTIONALSTATCUDA_H_

#include "Directionalstat.h"

namespace algorithm {
namespace cuda {

class Directionalstat : public algorithm::Directionalstat {
 public:
  Directionalstat(xt::xtensor<std::complex<float>, 4>& visibilities,
                  xt::xtensor<bool, 2>& flags, size_t time_step,
                  int device_id = 0);
  Directionalstat(xt::xtensor<std::complex<float>, 4>& visibilities,
                  xt::xtensor<bool, 2>& flags,
                  const std::vector<size_t> baseline_indices, size_t time_step,
                  int device_id = 0);
  ~Directionalstat();
  virtual void initialize() override;
  virtual void compute_statistics() override;
  virtual void compute_flags(float R_threshold) override;
  virtual void finish() override;

 private:
  void initialize_gpu();

  size_t device_id_ = 0;
  bool integrated_gpu_ = false;
  bool initialized_ = false;
  size_t time_step_ = 0;
  size_t* d_baseline_indices_ = nullptr;
  double* d_R_ = nullptr;
  std::complex<float>* d_visibilities_ = nullptr;
  bool* d_flags_ = nullptr;
};

}  // namespace cuda
}  // namespace algorithm

#endif  // DIRECTIONALSTATCUDA_H_