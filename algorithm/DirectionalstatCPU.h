#ifndef DIRECTIONALSTATCPU_H_
#define DIRECTIONALSTATCPU_H_

#include "Directionalstat.h"

namespace algorithm {
namespace cpu {

class Directionalstat : public algorithm::Directionalstat {
 public:
  Directionalstat(xt::xtensor<std::complex<float>, 4>& visibilities,
                  xt::xtensor<bool, 2>& flags, size_t time_step);
  Directionalstat(xt::xtensor<std::complex<float>, 4>& visibilities,
                  xt::xtensor<bool, 2>& flags,
                  const std::vector<size_t> baseline_indices, size_t time_step);
  virtual void compute_statistics() override;
  virtual void compute_flags(float R_threshold) override;

 private:
  size_t time_step_;  // number of timesteps per time window
  std::vector<size_t> baseline_indices_;
  xt::xtensor<double, 3>
      R_;  // statistics: (n_baseline_,n_windows_,n_channels_)
};
}  // namespace cpu
}  // namespace algorithm

#endif  // DIRECTIONALSTATCPU_H_
