#include <cuda_runtime.h>
#include <cuComplex.h>
#include <cuda_runtime_api.h>
#include <cooperative_groups.h>

namespace cg = cooperative_groups;

#include "common/detail.h"

#include "DirectionalstatCUDA.h"

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#define BLOCK_SIZE_X 128

#define cudaCheck(status)    \
  if (cudaSuccess != status) \
  throw detail::CUDAError(cudaGetErrorString(status), __FILE__, __LINE__)

namespace detail {
class CUDAError : public std::runtime_error {
  std::string message;

 public:
  CUDAError(const std::string& arg, const char* file, int line)
      : std::runtime_error(arg) {
    std::stringstream ss;
    ss << "CUDA error: " << arg << " at " << file << ":" << line;
    message = ss.str();
  }
  ~CUDAError() throw() {}
  const char* what() const throw() { return message.c_str(); }
};

size_t* initialize_baseline_indices(
    const std::vector<size_t>& baseline_indices) {
  const size_t n_baselines = baseline_indices.size();
  const size_t sizeof_baseline_indices = n_baselines * sizeof(size_t);
  size_t* d_baseline_indices;
  cudaCheck(
      cudaMalloc(&d_baseline_indices, n_baselines * sizeof_baseline_indices));
  cudaCheck(cudaMemcpy(d_baseline_indices, baseline_indices.data(),
                       sizeof_baseline_indices, cudaMemcpyHostToDevice));
  return d_baseline_indices;
}
}  // end namespace detail

namespace algorithm {
namespace cuda {

namespace kernel {
inline __device__ size_t index_visibilities(size_t n_time, size_t n_channels,
                                            size_t n_correlations, size_t bl,
                                            size_t time, size_t chan,
                                            size_t cor) {
  // visibilities[n_baselines][n_time][n_channels][n_correlations]
  return bl * n_time * n_channels * n_correlations +
         time * n_channels * n_correlations + chan * n_correlations + cor;
}

inline __device__ size_t index_r(size_t n_windows, size_t n_channels, size_t bl,
                                 size_t window, size_t chan) {
  // R[n_baselines][n_windows][n_channels]
  return bl * n_windows * n_channels + window * n_channels + chan;
}

inline __device__ cuFloatComplex operator+(const cuFloatComplex& a,
                                           const cuFloatComplex& b) {
  return {a.x + b.x, a.y + b.y};
}

inline __device__ cuFloatComplex operator-(const cuFloatComplex& a,
                                           const cuFloatComplex& b) {
  return {a.x - b.x, a.y - b.y};
}

inline __device__ void operator/=(cuFloatComplex& a, const float& b) {
  a.x /= b;
  a.y /= b;
}

inline __device__ cuFloatComplex operator*(const cuFloatComplex& a,
                                           const cuFloatComplex& b) {
  return {a.x * b.x, a.y * b.y};
}

// https://forums.developer.nvidia.com/t/additional-cucomplex-functions-cucnorm-cucsqrt-cucexp-and-some-complex-double-functions/36892
inline __device__ cuFloatComplex cuCsqrtf(const cuFloatComplex& x) {
  float radius = cuCabsf(x);
  float cosA = x.x / radius;
  cuFloatComplex out;
  out.x = sqrtf(radius * (cosA + 1.0) / 2.0);
  out.y = sqrtf(radius * (1.0 - cosA) / 2.0);
  // signbit should be false if x.y is negative
  if (signbit(x.y)) out.y *= -1.0;
  return out;
}

__device__ float reduce_sum(cooperative_groups::thread_group& group,
                            float* data, size_t n) {
  // Each iteration halves the number of active threads
  // Each thread adds its partial data[lane+i] to data[lane]
  for (size_t i = (n + 1) / 2; i > 0; i /= 2) {
    for (size_t j = 0; j < i; j += group.size()) {
      size_t lane = group.thread_rank() + j;
      group.sync();
      if (lane < i) {
        data[lane] += data[lane + i];
      }
      group.sync();
    }
  }
  return data[0];
}

__global__ void compute_statistics_kernel(size_t n_time, size_t n_windows,
                                          size_t time_step, size_t n_channels,
                                          size_t n_correlations,
                                          const cuFloatComplex* visibilities,
                                          double* R) {
  // thread indices
  cg::grid_group grid = cg::this_grid();
  cg::thread_block block = cg::this_thread_block();
  const unsigned int nr_threads = blockDim.x;
  const unsigned int lane = block.thread_rank();

  // data indices
  const size_t bl = blockIdx.x;
  const size_t time_window = blockIdx.y;
  const size_t chan = blockIdx.z;
  const size_t time_first = time_window * time_step;
  const size_t time_last = MIN(time_first + time_step - 1, n_time);
  time_step = time_last - time_first;

  // partial direction cosines
  __shared__ float zeta[3][BLOCK_SIZE_X];
  zeta[0][lane] = 0.0f;
  zeta[1][lane] = 0.0f;
  zeta[2][lane] = 0.0f;
  block.sync();

  // sum of direction cosines
  float zeta1 = 0;
  float zeta2 = 0;
  float zeta3 = 0;

  for (size_t i = lane; i < time_step; i += nr_threads) {
    const size_t time = time_first + i;
    size_t idx_xx = index_visibilities(n_time, n_channels, n_correlations, bl,
                                       time, chan, 0);
    size_t idx_xy = index_visibilities(n_time, n_channels, n_correlations, bl,
                                       time, chan, 1);
    size_t idx_yx = index_visibilities(n_time, n_channels, n_correlations, bl,
                                       time, chan, 2);
    size_t idx_yy = index_visibilities(n_time, n_channels, n_correlations, bl,
                                       time, chan, 3);
    const cuFloatComplex XX = visibilities[idx_xx];
    const cuFloatComplex XY = visibilities[idx_xy];
    const cuFloatComplex YX = visibilities[idx_yx];
    const cuFloatComplex YY = visibilities[idx_yy];

    // form Stokes
    cuFloatComplex QQ = XX - YY;
    cuFloatComplex UU = XY + YX;
    const cuFloatComplex temp = XY - YX;
    cuFloatComplex VV = {temp.y, temp.x};

    // norm
    cuFloatComplex PP =
        cuCsqrtf(cuCmulf(QQ, cuConjf(QQ)) + cuCmulf(UU, cuConjf(UU)) +
                 cuCmulf(VV, cuConjf(VV)));
    if (isnan(PP.x) || isnan(PP.y)) {
      PP = {0, 0};
    }

    // normalize
    QQ /= cuCabsf(PP) + 1e-6f;
    UU /= cuCabsf(PP) + 1e-6f;
    VV /= cuCabsf(PP) + 1e-6f;

    // extract point on Poincare sphere
    zeta1 += QQ.x;
    zeta2 += UU.x;
    zeta3 += VV.x;
  }

  if (lane < time_step) {
    zeta[0][lane] = zeta1;
    zeta[1][lane] = zeta2;
    zeta[2][lane] = zeta3;
  }
  block.sync();

  // reconstruct sum of direction cosines
  zeta1 = reduce_sum(block, zeta[0], nr_threads);
  zeta2 = reduce_sum(block, zeta[1], nr_threads);
  zeta3 = reduce_sum(block, zeta[2], nr_threads);

  // only the first thread computes R and stores the result
  if (lane == 0) {
    // vector sum of radii, normalized by number of samples
    const size_t n_samples = time_step + 1;
    const size_t idx_r = index_r(n_windows, n_channels, bl, time_window, chan);
    R[idx_r] = sqrtf(zeta1 * zeta1 + zeta2 * zeta2 + zeta3 * zeta3) / n_samples;
  }
}  // end compute_statistics kernel

__global__ void compute_flags_kernel(size_t* baseline_indices, double* R,
                                     bool* flags, float R_threshold,
                                     size_t n_time, size_t n_windows,
                                     size_t time_step, size_t n_channels) {
  // thread indices
  cg::thread_block block = cg::this_thread_block();
  const unsigned int nr_threads = blockDim.x;
  const unsigned int lane = block.thread_rank();

  const size_t bl = blockIdx.x;
  const size_t bl_dst = baseline_indices[bl];

  for (size_t time_first = 0; time_first < n_time; time_first += time_step) {
    const size_t time_last = MIN(time_first + time_step - 1, n_time);
    const size_t time_window = time_first / time_step;
    const size_t idx_r = index_r(n_windows, n_channels, bl, time_window, 0);
    if (R[idx_r] > R_threshold) {
      for (size_t time = time_first + lane; time < time_last;
           time += nr_threads) {
        flags[bl_dst * n_time + time] = true;
      }
    }
  }
}  // end compute_flags_kernel
}  // namespace kernel

Directionalstat::Directionalstat(
    xt::xtensor<std::complex<float>, 4>& visibilities,
    xt::xtensor<bool, 2>& flags, size_t time_step, int device_id)
    : ::algorithm::Directionalstat(visibilities, flags),
      device_id_(device_id),
      time_step_(time_step) {
  initialize_gpu();
  const size_t n_baselines = visibilities.shape(0);
  d_baseline_indices_ = detail::initialize_baseline_indices(
      detail::compute_baseline_selection(n_baselines));
}

Directionalstat::Directionalstat(
    xt::xtensor<std::complex<float>, 4>& visibilities,
    xt::xtensor<bool, 2>& flags, const std::vector<size_t> baseline_indices,
    size_t time_step, int device_id)
    : ::algorithm::Directionalstat(visibilities, flags),
      device_id_(device_id),
      time_step_(time_step) {
  initialize_gpu();
  d_baseline_indices_ = detail::initialize_baseline_indices(baseline_indices);
}

Directionalstat::~Directionalstat() {
  std::cout << "~Directionalstat()"
            << "\n";
  try {
    if (d_R_) {
      cudaCheck(cudaFree(d_R_));
    }
    if (d_baseline_indices_) {
      cudaCheck(cudaFree(d_baseline_indices_));
    }
    if (initialized_) {
      if (integrated_gpu_) {
        cudaCheck(cudaHostUnregister(visibilities_.data()));
      } else {
        cudaCheck(cudaFree(d_visibilities_));
      }
      cudaCheck(cudaHostUnregister(flags_.data()));
    }
  } catch (detail::CUDAError) {
  }
}

void Directionalstat::initialize_gpu() {
  cudaCheck(cudaSetDevice(device_id_));
  cudaDeviceProp prop;
  cudaCheck(cudaGetDeviceProperties(&prop, device_id_));
  std::cout << ">>> GPU"
            << "\n";
  std::cout << "CUDA device: " << prop.name << "\n";
  std::cout << "CUDA capability: " << prop.major << "." << prop.minor << "\n";
  std::cout << "Integrated GPU?: " << prop.integrated << "\n";
  std::cout << "\n";
  integrated_gpu_ = prop.integrated;
}

void Directionalstat::initialize() {
  if (initialized_) {
    return;
  }

  size_t sizeof_visibilities = visibilities_.size() * sizeof(cuFloatComplex);
  cudaCheck(
      cudaHostRegister(visibilities_.data(), sizeof_visibilities,
                       cudaHostRegisterMapped | cudaHostRegisterReadOnly));
  if (integrated_gpu_) {
    cudaCheck(
        cudaHostGetDevicePointer(&d_visibilities_, visibilities_.data(), 0));
  } else {
    cudaCheck(cudaMalloc(&d_visibilities_, sizeof_visibilities));
    cudaCheck(cudaMemcpyAsync(d_visibilities_, visibilities_.data(),
                              sizeof_visibilities, cudaMemcpyHostToDevice));
  }

  const size_t n_baselines = visibilities_.shape(0);
  const size_t n_time = visibilities_.shape(1);
  const size_t n_windows = (n_time + time_step_ - 1) / time_step_;
  const size_t n_channels = visibilities_.shape(2);

  const size_t sizeof_R = n_baselines * n_windows * n_channels * sizeof(double);
  if (d_R_) {
    cudaCheck(cudaFree(d_R_));
  }
  cudaCheck(cudaMalloc(&d_R_, sizeof_R));

  const size_t sizeof_flags = flags_.size() * sizeof(bool);
  cudaCheck(
      cudaHostRegister(flags_.data(), sizeof_flags, cudaHostRegisterMapped));

  cudaCheck(cudaHostGetDevicePointer(&d_flags_, flags_.data(), 0));
  initialized_ = true;
}

void Directionalstat::finish() { cudaCheck(cudaDeviceSynchronize()); }

void Directionalstat::compute_statistics() {
  if (!initialized_) {
    throw std::runtime_error("Call initialize() first.");
  }

  const size_t n_baselines = visibilities_.shape(0);
  const size_t n_time = visibilities_.shape(1);
  const size_t n_windows = (n_time + time_step_ - 1) / time_step_;
  const size_t n_channels = visibilities_.shape(2);
  const size_t n_correlations = visibilities_.shape(3);

  dim3 block(BLOCK_SIZE_X);
  dim3 grid(n_baselines, n_windows, n_channels);

  kernel::compute_statistics_kernel<<<grid, block>>>(
      n_time, n_windows, time_step_, n_channels, n_correlations,
      reinterpret_cast<cuFloatComplex*>(d_visibilities_), d_R_);
}  // end compute_statistics

void Directionalstat::compute_flags(float R_threshold) {
  if (!initialized_) {
    throw std::runtime_error("Call initialize() first.");
  }

  const size_t n_baselines = visibilities_.shape(0);
  const size_t n_time = visibilities_.shape(1);
  const size_t n_windows = (n_time + time_step_ - 1) / time_step_;
  const size_t n_channels = visibilities_.shape(2);

  dim3 block(BLOCK_SIZE_X);
  dim3 grid(n_baselines);

  kernel::compute_flags_kernel<<<grid, block>>>(
      d_baseline_indices_, d_R_, d_flags_, R_threshold, n_time, n_windows,
      time_step_, n_channels);

  const size_t sizeof_flags = flags_.size() * sizeof(bool);
  cudaCheck(cudaMemcpy(flags_.data(), d_flags_, sizeof_flags,
                       cudaMemcpyDeviceToHost));
}  // end compute_flags

}  // namespace cuda
}  // namespace algorithm
