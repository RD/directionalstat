#ifndef DIRECTIONALSTAT_H_
#define DIRECTIONALSTAT_H_

#include <vector>
#include <xtensor/xtensor.hpp>

namespace algorithm {

class Directionalstat {
 public:
  Directionalstat(xt::xtensor<std::complex<float>, 4>& visibilities,
                  xt::xtensor<bool, 2>& flags)
      : visibilities_(visibilities), flags_(flags) {}
  virtual void initialize(){};
  virtual void compute_statistics() = 0;
  virtual void compute_flags(float R_threshold) = 0;
  virtual void finish(){};

 protected:
  xt::xtensor<std::complex<float>, 4>& visibilities_;
  xt::xtensor<bool, 2>& flags_;
};
}  // namespace algorithm

#endif  // DIRECTIONALSTAT_H_
