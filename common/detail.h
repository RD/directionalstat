#include <cstddef>
#include <memory>
#include <utility>

#include <xtensor/xtensor.hpp>

#include "algorithm/DirectionalstatCPU.h"
#if defined(HAVE_CUDA)
#include "algorithm/DirectionalstatCUDA.h"

#endif
#if defined(HAVE_PMT)
#include <pmt.h>
#endif

namespace detail {

size_t compute_n_baselines(size_t n_antennas);

std::pair<size_t, size_t> compute_baseline(size_t n_antennas,
                                           size_t n_baselines,
                                           size_t baseline_index);

xt::xtensor<std::pair<size_t, size_t>, 1> compute_baselines(size_t n_antennas);

size_t compute_baseline_index(size_t antenna1, size_t antenna2);

xt::xtensor<double, 1> compute_uv_distance(const xt::xtensor<double, 3>& uvw,
                                           double lmd);

std::vector<size_t> compute_baseline_selection(size_t n_baseliens);

std::vector<size_t> compute_baseline_selection(
    const xt::xtensor<double, 3>& uvw,
    const xt::xtensor<std::pair<size_t, size_t>, 1>& baselines, double uv_limit,
    double lmd);

void report_rfi(const xt::xtensor<bool, 2>& flags_in,
                const xt::xtensor<bool, 2>& flags_out,
                size_t n_baselines_selected, size_t time_step);

std::unique_ptr<algorithm::Directionalstat> get_directionalstat(
    xt::xtensor<std::complex<float>, 4>& visibilities,
    xt::xtensor<bool, 2>& flags, size_t time_step);

std::unique_ptr<algorithm::Directionalstat> get_directionalstat(
    xt::xtensor<std::complex<float>, 4>& visibilities,
    xt::xtensor<bool, 2>& flags, const std::vector<size_t>& baseline_indices,
    size_t time_step);

#if defined(HAVE_PMT)
std::unique_ptr<pmt::PMT> get_pmt(int pmt_id = 0);
#endif

}  // namespace detail
