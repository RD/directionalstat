#include <algorithm>
#include <cmath>

#include "detail.h"

namespace detail {

size_t compute_n_baselines(size_t n_antennas) {
  size_t n_correlations = (n_antennas * (n_antennas - 1)) / 2;
  size_t n_autocorrelations = n_antennas;
  return n_correlations + n_autocorrelations;
}

std::pair<size_t, size_t> compute_baseline(size_t n_antennas,
                                           size_t n_baselines,
                                           size_t baseline_index) {
  const size_t antenna =
      (1 + std::sqrt(1 + 8 * (n_baselines - baseline_index - 1))) / 2;
  const size_t antenna1 = n_antennas - antenna;
  // n is the number of baselines (a,b) with a < antenna
  const size_t n = n_baselines - n_antennas - (antenna * (antenna - 1)) / 2;
  const size_t antenna2 = baseline_index - n;
  return {antenna1, antenna2};
}  // end compute_n_baselines

xt::xtensor<std::pair<size_t, size_t>, 1> compute_baselines(size_t n_antennas) {
  const size_t n_baselines = compute_n_baselines(n_antennas);

  xt::xtensor<std::pair<size_t, size_t>, 1> baselines({n_baselines});

  for (size_t bl = 0; bl < n_baselines; ++bl) {
    baselines[bl] = detail::compute_baseline(n_antennas, n_baselines, bl);
  }

  return baselines;
}  // end compute_baselines

size_t compute_baseline_index(size_t antenna1, size_t antenna2) {
  size_t row = std::min(antenna1, antenna2);
  size_t col = std::max(antenna1, antenna2);
  return static_cast<int>((col * (col + 1)) / 2 + row);
}  // end compute_baseline_index

xt::xtensor<double, 1> compute_uv_distance(const xt::xtensor<double, 3>& uvw,
                                           double lmd) {
  const size_t n_baselines = uvw.shape(0);
  const size_t n_time = uvw.shape(1);
  xt::xtensor<double, 1> uv_distance_mean({n_baselines}, 0);
  for (size_t bl = 0; bl < n_baselines; ++bl) {
    double uv_distance = 0;
    for (size_t time = 0; time < n_time; ++time) {
      const double u = uvw(bl, time, 0);
      const double v = uvw(bl, time, 1);
      const double w = uvw(bl, time, 2);
      uv_distance += sqrt(u * u + v * v + w * w);
    }  // end for time
    uv_distance_mean(bl) = uv_distance / (n_time * lmd);
  }  // end for bl
  return uv_distance_mean;
}  // end compute_uv_distance

std::vector<size_t> compute_baseline_selection(size_t n_baselines) {
  std::vector<size_t> baseline_indices(n_baselines);
  for (size_t bl = 0; bl < n_baselines; ++bl) {
    baseline_indices[bl] = bl;
  }
  return baseline_indices;
}  // end compute_baseline_selection

std::vector<size_t> compute_baseline_selection(
    const xt::xtensor<double, 3>& uvw,
    const xt::xtensor<std::pair<size_t, size_t>, 1>& baselines, double uv_limit,
    double frequency_first) {
  const size_t n_baselines = uvw.shape(0);
  assert(n_baselines == baselines.shape(0));

  const double lmd = 299792458.0 / frequency_first;  // wavelength
  const xt::xtensor<double, 1>& uv_distance =
      detail::compute_uv_distance(uvw, lmd);

  std::vector<size_t> selected_baselines;

  for (size_t bl = 0; bl < n_baselines; ++bl) {
    const size_t antenna1 = baselines(bl).first;
    const size_t antenna2 = baselines(bl).second;
    if ((antenna1 != antenna2) && (uv_distance(bl) > 0) &&
        (uv_distance(bl) <= uv_limit)) {
      selected_baselines.push_back(bl);
    }
  }

  return selected_baselines;
}  // end compute_baseline_selection

void report_rfi(const xt::xtensor<bool, 2>& flags_in,
                const xt::xtensor<bool, 2>& flags_out,
                size_t n_baselines_selected, size_t time_step) {
  const size_t n_baselines = flags_in.shape(0);
  const size_t n_time = flags_in.shape(1);
  assert(n_baselines == flags_out.shape(0));
  assert(n_time == flags_out.shape(1));
  const size_t n_windows = (n_time + time_step - 1) / time_step;

  std::vector<size_t> baseline_n_chunks(n_baselines);

  for (size_t bl = 0; bl < n_baselines; ++bl) {
    for (size_t time = 0; time < n_time; time += time_step) {
      if (!flags_in(bl, time) && flags_out(bl, time)) {
        baseline_n_chunks[bl]++;
      }
    }  // end for time_first
  }    // end for bl

  // number of baselines/chunks with rfi
  size_t n_baselines_rfi = 0;
  size_t n_chunks_rfi = 0;
  for (size_t count : baseline_n_chunks) {
    n_baselines_rfi += count > 0;
    n_chunks_rfi += count;
  }

  std::cout << "Inspected " << n_baselines_selected << " out of " << n_baselines
            << " baselines\n";
  std::cout << "Found RFI in " << n_baselines_rfi << " baselines\n";
  std::cout << "Found RFI in " << n_chunks_rfi << " out of "
            << n_baselines_selected * n_windows << " windows\n";
}  // end report_rfi

std::unique_ptr<algorithm::Directionalstat> get_directionalstat(
    xt::xtensor<std::complex<float>, 4>& visibilities,
    xt::xtensor<bool, 2>& flags, const std::vector<size_t>& baseline_indices,
    size_t time_step) {
#if defined(HAVE_CUDA)
  bool use_cuda = getenv("USE_CUDA") != nullptr;
  if (use_cuda) {
    char* cuda_device_char = getenv("CUDA_DEVICE");
    int cuda_device_id = cuda_device_char ? atoi(cuda_device_char) : 0;
    return std::unique_ptr<algorithm::Directionalstat>(
        new algorithm::cuda::Directionalstat(
            visibilities, flags, baseline_indices, time_step, cuda_device_id));
  }
#endif
  return std::unique_ptr<algorithm::Directionalstat>(
      new algorithm::cpu::Directionalstat(visibilities, flags, baseline_indices,
                                          time_step));
}

std::unique_ptr<algorithm::Directionalstat> get_directionalstat(
    xt::xtensor<std::complex<float>, 4>& visibilities,
    xt::xtensor<bool, 2>& flags, size_t time_step) {
#if defined(HAVE_CUDA)
  char* use_cuda_char = getenv("USE_CUDA");
  bool use_cuda = use_cuda_char != nullptr ? atoi(use_cuda_char) : false;
  if (use_cuda) {
    char* cuda_device_char = getenv("CUDA_DEVICE");
    int cuda_device_id = cuda_device_char ? atoi(cuda_device_char) : 0;
    return std::unique_ptr<algorithm::Directionalstat>(
        new algorithm::cuda::Directionalstat(visibilities, flags, time_step,
                                             cuda_device_id));
  }
#endif
  return std::unique_ptr<algorithm::Directionalstat>(
      new algorithm::cpu::Directionalstat(visibilities, flags, time_step));
}

#if defined(HAVE_PMT)
std::unique_ptr<pmt::PMT> get_pmt(int pmt_id) {
  std::string pmt_str =
      pmt_id > 0 ? "PMT_DEVICE_" + std::to_string(pmt_id) : "PMT_DEVICE";
  const char* pmt_device = std::getenv(pmt_str.c_str());
#if defined(HAVE_PMT_NVML)
  if (pmt_device && std::string(pmt_device).compare("nvml") == 0) {
    return pmt::nvml::NVML::create();
  }
#endif
  if (pmt_device && std::string(pmt_device).compare("jetson") == 0) {
    return pmt::jetson::Jetson::create();
  } else if (pmt_device && std::string(pmt_device).compare("rapl") == 0) {
    return pmt::rapl::Rapl::create();
  } else {
    return pmt::Dummy::create();
  }
}
#endif

}  // namespace detail
